import sqlite3

class DB:
    """ Fast connection to the database using context management.

    :param dbpath: Filepath to the sqlite database
    :type dbpath: str
    """

    def __init__(self, dbpath = None):
        self.dbpath = dbpath

    def __enter__(self):
        self.conn = sqlite3.connect(self.dbpath)
        self.conn.row_factory = sqlite3.Row
        return self.conn.cursor()

    def __exit__(self, type, value, traceback):
        self.conn.commit()
        self.conn.close()


def init(dbpath):
    """ Initialise the database if it does not exist yet

    :param dbpath: Filepath to the sqlite database
    :type dbpath: str
    """

    with DB(dbpath) as cursor:
        cursor.execute('''CREATE TABLE IF NOT EXISTS crossposts (
                            toot_id   INTEGER  PRIMARY KEY
                                            UNIQUE
                                            NOT NULL,
                            tweet_id  INTEGER  UNIQUE
                                            NOT NULL,
                            posted_at DATETIME DEFAULT (CURRENT_TIMESTAMP));''')

        cursor.execute('''CREATE TABLE IF NOT EXISTS toots (
                            toot_id      INTEGER          PRIMARY KEY
                                                        UNIQUE
                                                        NOT NULL,
                            status       VARCHAR (1, 255) NOT NULL,
                            processed_at DATETIME         DEFAULT (CURRENT_TIMESTAMP));''')
