import bs4
import datetime
import importlib
import logging
import math
import os
import re
import requests
import tempfile
import textcut

from twitter_text import Validation as TwitterCheck

from . import auth
from . import database
from . import resources
from .chrono import Chrono

class CrossPoster:

    def __init__(self, config):
        logging.info("Loading mastodon API...")
        self.mastodon     = auth.MastodonAPI(dict(config['mastodon']))
        self.mastodon_me  = self.mastodon.me()

        logging.info("Loading twitter API...")
        self.twitter    = auth.TwitterAPI(dict(config['twitter']))
        self.twitter_me = self.twitter.get_me().data

        logging.info("Loading database...")
        self.dbpath   = os.path.expanduser(config.get("app", "database"))
        database.init(self.dbpath)

        logging.info("Loading configuration...")
        self.visibilities = [x.strip().lower()
                             for x in config.get("app", "visibilities", fallback = "public").split(" ")]

        self.cfg = config['app']

        logging.info("Initialisation...")
        with database.DB(self.dbpath) as cur:
            res = cur.execute(
                    'SELECT toot_id FROM toots ORDER BY toot_id DESC LIMIT 1').fetchone()

            self.last_id = max(
                    config.getint('app', 'since id', fallback = 0),
                    0 if res is None else res['toot_id'])

        logging.info("Ready to serve!")


    def monitor(self):
        while True:
            chrono = Chrono()
            self.once()
            chrono.wait(self.cfg.getint('period'))


    def once(self):
        toots = self.mastodon.account_statuses(
                self.mastodon_me['id'],
                exclude_reblogs = True,
                min_id = self.last_id)

        with database.DB(self.dbpath) as cur:
            for toot in toots[::-1]:
                self.last_id = toot['id']

                if toot['visibility'] not in self.visibilities:
                    self.set_toot_processing_status(cur, toot['id'], 'skip:visibility')
                    continue

                if self.get_crosspost(cur, toot['id']) is not None:
                    self.set_toot_processing_status(cur, toot['id'], 'done')
                    continue

                logging.info(f"🐘 Found a new toot ({toot['id']})")
                if self.crosspost_exclusion(toot):
                    logging.info("    ↳ Should not cross-post, skipping...")
                    self.set_toot_processing_status(cur, toot['id'], 'skip:exclusion')
                    continue

                in_reply_to_tweet_id = None
                if toot['in_reply_to_id'] is not None:
                    parent = self.get_crosspost(cur, toot['in_reply_to_id'])
                    if parent is None:
                        logging.info("    ↳ Parent not on Twitter, ignoring...")
                        self.set_toot_processing_status(cur, toot['id'], 'skip:orphan')
                        continue

                    in_reply_to_tweet_id = parent['tweet_id']
                    logging.info(f"    ↳ In reply to tweet {in_reply_to_tweet_id}")

                poll_options = self.get_poll_options(toot)
                if poll_options is None:
                    logging.info("    ↳ Poll option not compatible with Twitter, ignoring...")
                    self.set_toot_processing_status(cur, toot['id'], 'skip:bad poll')
                    continue

                elif poll_options['poll_options'] is not None:
                    logging.info(f"    ↳ Adding a poll with {len(poll_options['poll_options'])} items")

                media_group_ids = [[]]
                on_error  = False
                medias = [{"url": x['url'], "description": x['description'], "type": x['type'], "continues": False}
                            for x in toot['media_attachments']
                            if x['type'] in ("image", "video")]

                while len(medias) > 0:
                    media_attachments = medias[0]

                    url       = media_attachments['url']
                    alt       = media_attachments['description']
                    continues = media_attachments['continues']
                    mtype     = media_attachments['type']

                    if url is None and continues:
                        tmpfile = str(importlib.resources.files(resources).joinpath("alt-text.png"))
                    else:
                        img_data = requests.get(url).content
                        tmpfd, tmpfile = tempfile.mkstemp(suffix = os.path.basename(url))
                        os.write(tmpfd, img_data)
                        os.close(tmpfd)

                    media_count = sum([len(x) for x in media_group_ids])
                    try:
                        logging.info(f"    ↳ Uploading media #{media_count+1}...")
                        media = self.twitter.media_upload(
                                filename = tmpfile,
                                chunked = mtype == 'video')

                        if mtype == 'video':
                            if len(media_group_ids[-1]) > 0:
                                media_group_ids += [[]]

                            media_group_ids[-1] += [media.media_id_string]
                            media_group_ids     += [[]]
                        else:
                            if len(media_group_ids[-1]) == 4:
                                media_group_ids += [[]]

                            media_group_ids[-1] += [media.media_id_string]

                            if alt is not None and alt != "":
                                alt_count = math.ceil(len(alt) / 1000)
                                if alt_count == 1:
                                    self.twitter.create_media_metadata(media.media_id_string, alt)
                                else:
                                    alt_suffix = " [alt-text continues]"
                                    cutter = textcut.TextCut(tolerance = 0.7,
                                                             width = 1000 - len(alt_suffix))
                                    parts  = cutter.wrap(alt)
                                    parts  = [x + alt_suffix for x in parts[:-1]] + [parts[-1]]

                                    self.twitter.create_media_metadata(media.media_id_string, parts[0])
                                    medias = ([medias[0]] +
                                                [{"url": None, "description": x, "type": "image", "continues": True}
                                                for x in parts[1:]] +
                                                medias[1:])

                        medias = medias[1:]

                    except Exception as ex:
                        logging.error("*** Media upload error: " + str(ex))
                        if not self.cfg.getboolean('media_ignore_errors'):
                            on_error = True
                            break

                if on_error:
                    logging.info("    ↳ Error ☠️")
                    self.set_toot_processing_status(cur, toot['id'], 'error:media')
                    continue

                media_group_ids = [x for x in media_group_ids if len(x) > 0]
                text_parts      = self.text_for_twitter(toot)
                if poll_options['poll_options'] is not None:
                    media_group_ids = [None] + media_group_ids

                if len(media_group_ids) > len(text_parts):
                    media_only  = len(media_group_ids) - len(text_parts)
                    text_parts += [""] * media_only
                    logging.info(f"    ↳ Text was broken in {len(text_parts)} parts, among which {media_only} just for pictures and videos")
                else:
                    text_only        = len(text_parts) - len(media_group_ids)
                    media_group_ids += [None for i in range(text_only)]
                    logging.info(f"    ↳ Text was broken in {len(text_parts)} part(s)")

                # Here:
                # - media_group_ids and text_parts have the same length
                # - if there is a poll, the first media_group_ids is None (media not allowed with polls on Twitter)
                # - if there is not enough text for the media, empty tweets will be added with media only
                # - if there is more text than image, last tweets won't have images
                # - videos have there own media_group
                # - images share the same media_group up to 4 images
                for ti in range(len(text_parts)):
                    tweet = self.twitter.create_tweet(
                                media_ids = media_group_ids[ti],
                                in_reply_to_tweet_id = in_reply_to_tweet_id,
                                text = text_parts[ti],
                                user_auth = True,
                                **poll_options)

                    poll_options = {"poll_duration_minutes": None, "poll_options": None}
                    in_reply_to_tweet_id = tweet.data['id']
                    logging.info(f"    ↳ Tweet {tweet.data['id']} published")

                cur.execute(
                        'INSERT INTO crossposts (toot_id, tweet_id) VALUES (?, ?)',
                        (toot['id'], tweet.data['id']))

                self.set_toot_processing_status(cur, toot['id'], 'done')
                logging.info("    ↳ Done 🐥")


    def get_poll_options(self, toot):
        if toot['poll'] is None:
            return {"poll_duration_minutes": None, "poll_options": None}

        multiple = toot['poll']['multiple']
        if multiple:
            if self.cfg.getboolean("poll_force_multiple") is not True:
                return None

        options = [x['title'] for x in toot['poll']['options']]
        if len(options) > 4:
            return None

        if self.cfg.getboolean("poll_synchronise") is True:
            now      = datetime.datetime.now(tz = toot['poll']['expires_at'].tzinfo)
            duration = (toot['poll']['expires_at'] - now).total_seconds() / 60
        else:
            duration = (toot['poll']['expires_at'] - toot['created_at']).total_seconds() / 60

        for i in range(len(options)):
            if len(options[i]) > 25:
                options[i] = options[i][:24] + "…"

        duration = int(max(5, min(10080, duration)))

        return {"poll_duration_minutes": duration, "poll_options": options}


    def crosspost_exclusion(self, toot):
        content = toot['content']
        content = content.replace(r'<br />', "\n")
        content = content.replace(r'</p><p>', "\n\n")

        soup = bs4.BeautifulSoup(content, "html.parser")
        text = soup.text

        pattern = self.cfg.get("exclusion tag", fallback = None).strip('"')
        if pattern is None:
            return False

        exclusion = re.findall(pattern, text)
        if len(exclusion) > 0:
            text = re.sub(pattern, "", text)

            try:
                self.mastodon.status_update(toot['id'], text)
            except:
                pass

            return True

        return False


    def text_for_twitter(self, toot):
        cw      = toot['spoiler_text']
        content = toot['content']
        content = content.replace(r'<br />', "\n")
        content = content.replace(r'</p><p>', "\n\n")

        # Force mastodon namespace on Twitter
        for mention in toot['mentions']:
            url     = mention['url']
            account = re.sub(r'https?://([^/]+)/@(.+)', r'@\2@\1', url)
            content = re.sub(f'<a href="{url}" .*?</a>', f'{account}', content)

        soup = bs4.BeautifulSoup(content, "html.parser")
        text = soup.text

        prefix_continue = self.cfg.get("prefix continue", fallback = "… ").strip('"')
        suffix_continue = self.cfg.get("suffix continue", fallback = " …").strip('"')
        no_affix_after  = self.cfg.get("no affix after",  fallback = "").strip('"')

        header    = '' if cw == '' else f"CW : {cw}\n\n"
        maxlength = self.cfg.getint('tweet length', fallback = 273) - len(header)

        # Remove twitter namespace when on Twitter
        text = re.sub(
                r'(\W@[a-z0-9_]{2,15})@twitter.com\b',
                r'\1', text,
                flags = re.IGNORECASE)

        # If a link in the toot refers to a cross-posted toot,
        # then we replace it by a link pointing to the cross-posted tweet.
        match = re.search(f"{self.mastodon_me['url']}/([0-9]+)", text)
        pos   = 0
        while match is not None:
            toot_id  = int(match.group(1))
            tweet_id = None

            with database.DB(self.dbpath) as cur:
                db_data = self.get_crosspost(cur, toot_id)
                if db_data is not None:
                    tweet_id = db_data['tweet_id']

                    toot_url  = f"{self.mastodon_me['url']}/{toot_id}"
                    tweet_url = f"https://twitter.com/{self.twitter_me.username}/status/{tweet_id}"

                    text = text.replace(toot_url, tweet_url)
                    pos += match.end() - len(toot_url) + len(tweet_url)
                else:
                    pos += match.end()

            match = re.search(f"{self.mastodon_me['url']}/([0-9]+)", text[pos:])

        cutter = textcut.TextCut(tolerance = 0.7,
                                 width = maxlength,
                                 len_func = lambda x: TwitterCheck(x).tweet_length(),
                                 normalise = True)
        parts  = cutter.wrap(text)

        return ["{h}{p}{t}{s}".format(
                        h = header,
                        p = '' if (i == 0 or (len(parts[i-1]) > 0 and parts[i-1][-1] in no_affix_after)) else prefix_continue,
                        t = parts[i],
                        s = '' if (i == len(parts)-1 or (len(parts[i]) > 0 and parts[i][-1] in no_affix_after)) else suffix_continue)
                for i in range(len(parts))]


    def get_crosspost(self, cursor, toot_id):
        res = cursor.execute(
                'SELECT * FROM crossposts WHERE toot_id = ?',
                (toot_id, )).fetchone()

        return res

    def set_toot_processing_status(self, cursor, toot_id, satus):
        res = cursor.execute(
                'INSERT INTO toots (toot_id, status) VALUES (?, ?)',
                (toot_id, satus))

        return res


