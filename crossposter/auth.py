import os
import tweepy
from mastodon import Mastodon

class MastodonAPI(Mastodon):
    """
    Wrapper around the Mastodon API to accomodate for automatic login.

    :param credentials: A dictionnary containing all the information to log in
        automatically. The dictionnary must contain the following keys:
        - client cred file: path to the client credential file (created if does not exist)
        - user cred file: path to the user credential file (created if does not exist)
        - base url: URL of the mastodon instance
        - application name: name of the application
        - email: email to log-in the account used by the application
        - password: password used by the application to login

        The application name is mandatory only the first time, to register the application
        and create the client credential file. The account and password is used the first time
        to create the user credential file. The base URL is always mandatory, to create the client
        credential file, the user credential file and to authenticate. A properly formed user
        credential file is required to authenticate. A properly formed client credential file is
        required to generate the user credential file. If the files do not exist, and assuming
        the mandatory properties are provided, they will be created.
    :type credentials: dict
    """


    def __init__(self, credentials):
        client_cred_file = os.path.expanduser(credentials['client cred file'])
        user_cred_file   = os.path.expanduser(credentials['user cred file'])
        if not os.path.exists(client_cred_file):
            Mastodon.create_app(
                credentials['application name'],
                api_base_url = credentials['base url'],
                to_file = client_cred_file
            )

        if not os.path.exists(user_cred_file):
            mastodon = Mastodon(
                client_id = client_cred_file,
                api_base_url = credentials['base url']
            )

            mastodon.log_in(
                credentials['email'],
                credentials['password'],
                to_file = user_cred_file
            )

        super().__init__(
            access_token = user_cred_file,
            api_base_url = credentials['base url']
        )


class TwitterAPI(tweepy.Client):
    """
    Wrapper around the Tweepy API to accomodate for automatic login.

    :param credentials: A dictionnary containing all the information to log in
        automatically. The dictionnary must contain the following keys:
        - key: the account public key provided by Twitter
        - key secret: the account private key provided by Twitter
        - acces token: the public application access token provided by Twitter
        - access token secret: the private application access token provided by Twitter

        All the properties are mandatory. They can be obtained when creating a
        developper account on Twitter, and creating an application.
    :type credentials: str
    """

    def __init__(self, credentials):
        super().__init__(
            consumer_key        = credentials['key'],
            consumer_secret     = credentials['key secret'],
            access_token        = credentials['access token'],
            access_token_secret = credentials['access token secret'],
            wait_on_rate_limit  = True
        )

        auth_v1 = tweepy.OAuth1UserHandler(
            credentials['key'],
            credentials['key secret'],
            credentials['access token'],
            credentials['access token secret'])

        self.__api_v1 = tweepy.API(auth_v1)


    def media_upload(self, *args, **kwargs):
        return self.__api_v1.media_upload(*args, **kwargs)

    def create_media_metadata(self, *args, **kwargs):
        return self.__api_v1.create_media_metadata(*args, **kwargs)
