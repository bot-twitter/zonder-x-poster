import time

class Chrono:

    @property
    def beginning(self):
        return self._beginning


    @property
    def duration(self):
        return time.time() - self._beginning


    def __init__(self):
        self._beginning = time.time()


    def restart(self):
        self._beginning = time.time()


    def wait(self, duration):
        left = duration - (time.time() - self._beginning)
        if left > 0:
            time.sleep(left)

