import argparse
import configparser
import logging

from .core import CrossPoster

logging.basicConfig(format = "[%(asctime)s]: %(levelname)s: %(message)s", datefmt='%Y-%m-%d %H:%M:%S', level=logging.INFO)

parser = argparse.ArgumentParser()
parser.add_argument("config", help = "The configuration file to use to configure the application.")
args = parser.parse_args()

config = configparser.ConfigParser()
config.read([args.config])

xposter = CrossPoster(config)

try:
    xposter.monitor()
except Exception as ex:
    logging.error("An unexpected error occured, quitting now...")
    logging.error(f"Error is: {ex}")

