# Zonder-X-Poster

Cross-post toot from Mastodon to Twitter.

## What it does

- Cross-post toots to Twitter,
- Breaks toots in multiple tweets if needed,
- Cross-post images with alt-text,
- Long alt-text are split in multiple pictures if necessary,
- Picture-only tweets will be added if there are more than 4 pics in one toot,
- Content-Warning is automatically added at the top of the tweet,
- Twitter handler (@username@twitter.com) are converted to @username,
- Toot links that have been cross-posted are converted to the corresponding tweets,
- Polls are cross-posted if possible,
- Ignore toots with a special "no-crosspost" text of your choice,
- The text preventing cross-posting is automatically edited out from the toot.

## What it does not do

- Videos can't be cross-posted (buggy),
- Cross-post reblogs (by design).

## Install

Just clone the repo and put it anywhere you want.

## Configuration

Open the `default_config.cfg` file and edit the content.

## Running the cross-poster

```bash
python -m crossposter config.cfg
```

